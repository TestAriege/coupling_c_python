################################################################################
#
# Copyright 1993-2006 NVIDIA Corporation.  All rights reserved.
#
# NOTICE TO USER:   
#
# This source code is subject to NVIDIA ownership rights under U.S. and 
# international Copyright laws.  
#
# NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE 
# CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR 
# IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF 
# MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.   
# IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, 
# OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
# OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
# OR PERFORMANCE OF THIS SOURCE CODE.  
#
# U.S. Government End Users.  This source code is a "commercial item" as 
# that term is defined at 48 C.F.R. 2.101 (OCT 1995), consisting  of 
# "commercial computer software" and "commercial computer software 
# documentation" as such terms are used in 48 C.F.R. 12.212 (SEPT 1995) 
# and is provided to the U.S. Government only as a commercial end item.  
# Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through 
# 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the 
# source code with only those rights set forth herein.
#
################################################################################
#
# Common build script
#
################################################################################

.SUFFIXES : .c_dbg_o .cpp_dbg_o .c_rel_o .cpp_rel_o


# Basic directory setup for SDK
# (override directories only if they are not already defined)
ROOTDIR    ?= .
SRCDIR     ?= $(ROOTDIR)/src/
ROOTBINDIR ?= $(ROOTDIR)/bin
BINDIR     ?= $(ROOTBINDIR)
ROOTOBJDIR ?= obj
LIBDIR     := $(ROOTDIR)/lib
COMMONDIR  := $(ROOTDIR)

# Compilers
ifeq ($(wos),mac)
CXX        := /usr/local/bin/g++-9
#CXX        := /usr/local/opt/llvm/bin/clang
CC	   := /usr/local/bin/gcc-9
#CC         := /usr/local/opt/llvm/bin/clang
ifeq ($(womp),yes)
LINK       := /usr/local/bin/g++-9 -fPIC -m64 -fopenmp
#LINK       := /usr/local/opt/llvm/bin/clang -fPIC -m64 -I/usr/local/opt/llvm/include -fopenmp
else
LINK       := /usr/local/bin/g++-9 -fPIC -m64
#LINK       := /usr/local/opt/llvm/bin/clang -fPIC -m64
endif
else
CXX        := g++ 
CC         := gcc 
ifeq ($(womp),yes)
LINK       := g++ -fPIC -m64 -fopenmp
else
LINK       := g++ -fPIC -m64 
endif
endif

# Includes

INCLUDES  += -I. -I$(CUDA_INSTALL_PATH)/include -I$(COMMONDIR)/inc -I$(CUDA_INSTALL_PATH)/samples/common/inc -I$(ROOTDIR)/../libigl/include 
ifeq ($(wpython),yes)
CXX_INCLUDES += -I. -I/usr/include/python3.5m
else
CXX_INCLUDES += -I. 
endif

# architecture flag for cubin build
CUBIN_ARCH_FLAG := -m64

# Warning flags
CXXWARN_FLAGS := \
	-W -Wall \
	-Wswitch \
	-Wformat \
	-Wchar-subscripts \
	-Wparentheses \
	-Wmultichar \
	-Wtrigraphs \
	-Wpointer-arith \
	-Wcast-align \
	-Wreturn-type \
	-Wno-unused-function \
	-std=c++0x\
	$(SPACE)

CWARN_FLAGS := $(CXXWARN_FLAGS) \
	-Wstrict-prototypes \
	-Wmissing-prototypes \
	-Wmissing-declarations \
	-Wnested-externs \
	-Wmain \


#ifeq ($(wmac),yes)
#CXXFLAGS := $(CXXFLAGS) -I/usr/local/opt/llvm/include
#endif

ifeq ($(womp),yes)
CXXFLAGS := $(CXXFLAGS) -fopenmp
endif

#ifeq ($(wmac),yes)
#CXXFLAGS := $(CXXFLAGS) -L/usr/local/opt/llvm/lib
#endif

# Common flags
COMMONFLAGS += $(INCLUDES) -DUNIX

# Debug/release configuration
ifeq ($(dbg),0)
	COMMONFLAGS += -g -G -O1
	NVCCFLAGS   += -D_DEBUG
	BINSUBDIR   := debug
	LIBSUFFIX   := D
	ifeq ($(emu), 1)
		EXESUFFIX   := _ed
	else
		EXESUFFIX   := _d
	endif
else 
	COMMONFLAGS += -g -O2
	BINSUBDIR   := release
	LIBSUFFIX   :=
	NVCCFLAGS   += --compiler-options -fno-strict-aliasing
	CXXFLAGS    += -fno-strict-aliasing
	CFLAGS      += -fno-strict-aliasing
	ifeq ($(emu), 1)
		EXESUFFIX   := _e
	else
		EXESUFFIX   := 
	endif
endif

# detect if 32 bit or 64 bit system
HP_64 =	$(shell uname -m | grep 64)

# OpenGL is used or not (if it is used, then it is necessary to include GLEW)
OPENGLLIB := -lGL -lGLU #-lglut
ifeq ($(USEGLLIB),1)
	ifeq "$(strip $(HP_64))" ""
		OPENGLLIB += -lGLEW
	else
		OPENGLLIB += -lGLEW_x86_64
	endif

	OPENGLLIB = -lGLEW


	CUBIN_ARCH_FLAG := -m64
endif

ifeq ($(USEPARAMGL),1)
	PARAMGLLIB := -lparamgl$(LIBSUFFIX)
endif

ifeq ($(USECUDPP), 1)
	ifeq "$(strip $(HP_64))" ""
		CUDPPLIB := -lcudpp
	else
		CUDPPLIB := -lcudpp64
	endif


	CUDPPLIB = -lcudpp


	CUDPPLIB := $(CUDPPLIB)$(LIBSUFFIX)

	ifeq ($(emu), 1)
		CUDPPLIB := $(CUDPPLIB)_emu
	endif
endif

# Libs
LIB       := -L$(LIBDIR) -L$(COMMONDIR)/lib -lgomp
ifeq ($(xmac),yes)
LIB += -L/usr/local/opt/llvm/lib
endif

# Lib/exe configuration
ifneq ($(STATIC_LIB),)
	TARGETDIR := $(LIBDIR)
	TARGET   := $(subst .a,$(LIBSUFFIX).a,$(LIBDIR)/$(STATIC_LIB))
	LINKLINE  = ar qv $(TARGET) $(OBJS) 
else
	#LIB += -lcutil_x86_64 #sm
	#LIB += -libcutil.a
	
	# Device emulation configuration
	ifeq ($(emu), 1)
		NVCCFLAGS   += -deviceemu
		CUDACCFLAGS += 
		BINSUBDIR   := emu$(BINSUBDIR)
		# consistency, makes developing easier
		CXXFLAGS		+= -D__DEVICE_EMULATION__
		CFLAGS			+= -D__DEVICE_EMULATION__
	endif
	TARGETDIR := $(BINDIR)
	TARGET    := $(TARGETDIR)/$(EXECUTABLE)$(EXESUFFIX)
	LINKLINE  = $(LINK) -o $(TARGET) $(OBJS) $(LIB)
endif

ifeq ($(wpython),yes)
LDFLAGS := $(LDFLAGS) -export-dynamic -L/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu -lpython3.5m -lpthread -lm -ldl -lutil
else
LDFLAGS :=
endif
#ifeq ($(wmac),yes)
#LDFLAGS := $(LDFLAGS) -L/usr/local/opt/llvm/lib
#endif

# check if verbose 
ifeq ($(verbose), 1)
	VERBOSE :=
else
	VERBOSE := @
endif

################################################################################
# Check for input flags and set compiler flags appropriately
################################################################################

CXXFLAGS  += $(COMMONFLAGS)
CFLAGS    += $(COMMONFLAGS)


################################################################################
# Set up object files
################################################################################
OBJDIR := $(ROOTOBJDIR)/$(BINSUBDIR)
OBJS +=  $(patsubst %.cpp,$(OBJDIR)/%.cpp_o,$(notdir $(CCFILES)))
OBJS +=  $(patsubst %.c,$(OBJDIR)/%.c_o,$(notdir $(CFILES)))


################################################################################
# Rules
################################################################################
#%.cpp : $(SRCDIR)%.cu
#	@echo "copy .cpp"
#	$(VERBOSE)cp -f $< $@
	

$(OBJDIR)/%.c_o : $(SRCDIR)%.c $(C_DEPS)
	@echo "les points c"
	@echo $(VERBOSE)$(CC) $(CFLAGS) -o $@ -c $<
	$(VERBOSE)$(CC) $(CFLAGS) -o $@ -c $<

$(OBJDIR)/%.cpp_o : $(SRCDIR)%.cpp $(C_DEPS)
	@echo "les points cpp"
	@echo $(VERBOSE)$(CXX) $(CXXFLAGS) $(CXX_INCLUDES) -o $@ -c $<
	$(VERBOSE)$(CXX) $(CXXFLAGS) $(CXX_INCLUDES) -o $@ -c $<

ifeq ($(wos),win10)
$(TARGET): $(OBJS) Makefile
	@echo "target"
	@echo $(VERBOSE)$(LINK) -o $(TARGET) $(OBJS) $(LDFLAGS)
	$(VERBOSE)$(LINK) -o $(TARGET) $(OBJS) $(LDFLAGS)
else
$(TARGET): makedirectories $(OBJS) Makefile
	@echo "target"
	@echo $(VERBOSE)$(LINK) -o $(TARGET) $(OBJS) $(LDFLAGS)
	$(VERBOSE)$(LINK) -o $(TARGET) $(OBJS) $(LDFLAGS)
endif



makedirectories:
	@mkdir -p $(LIBDIR)
	@mkdir -p $(OBJDIR)
	@mkdir -p $(TARGETDIR)


#tidy :
#	@find | egrep "#" | xargs rm -f
#	@find | egrep "\~" | xargs rm -f

clean : 
	$(VERBOSE)rm -f $(OBJS)
	$(VERBOSE)rm -f $(TARGET)

clobber : clean
	rm -rf $(ROOTOBJDIR) $(ROOTBINDIR)

