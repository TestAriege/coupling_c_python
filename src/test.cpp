#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <Python.h>
#include "pyhelper.hpp"

int main()
{
	//Crée l'instance Python
	CPyInstance hInstance;
	//~ Py_Initialize();
	
	//Essaye de travailler proprement en déclarant à l'avance les objets Pythons
	CPyObject px_vector, px_vector2, py_vector, py_vector2, pArgTuple, pValue;

	// Set PYTHONPATH TO working directory
    PyObject * sys = PyImport_ImportModule("sys");
    PyObject * path = PyObject_GetAttrString(sys, "path");
    PyList_Append(path, PyUnicode_FromString("src/."));
	
	
	//Charge le script Python contenant la fonction qui nous interesse sous forme de module
	CPyObject pName = PyUnicode_FromString("pyTest");
    CPyObject pModule = PyImport_Import(pName);
	
	//Controle que le chargement de module s'est bien passé
	if(pModule)
	{
		//Charge la fonction python qui nous interesse
		CPyObject pFunc = PyObject_GetAttrString(pModule, "getInteger");
		//Controle que la fonction est bien chargée et qu'il s'agit bien d'un objet Python appellable, ie une fonction python
		if(pFunc && PyCallable_Check(pFunc))
		{
			
			CPyObject pValue = PyObject_CallObject(pFunc, NULL);

			printf("C: getInteger() = %lf\n", PyFloat_AsDouble(pValue));
		}
		else
		{
			printf("ERROR: function getInteger()\n");
			PyErr_Print();
		}
		
		//Test avec une autre fonction qui appelle une fonction d'un autre module Python
		printf("Test mult:\n");
		CPyObject pFunc2 = PyObject_GetAttrString(pModule, "getMult");
		if(pFunc2 && PyCallable_Check(pFunc2))
		{
			//test: tuple simple avec 2 doubles
			CPyObject pARGS = PyTuple_New(2);
			
			CPyObject pVal1 = PyFloat_FromDouble(2.0);
			CPyObject pVal2 = PyFloat_FromDouble(5.0);
			
			PyTuple_SetItem(pARGS,0,pVal1); 
			PyTuple_SetItem(pARGS,1,pVal2); 
			
			CPyObject pValue2 = PyObject_CallObject(pFunc2, pARGS);
			
			//Test de la réussite de la fonction Python
			//Car sinon aucun message n'est indiqué
			if(pValue2 != NULL)
			{
				pVal1 = PyTuple_GetItem(pValue2, 1);
				printf("C : getMult() = %lf\n", PyFloat_AsDouble(pVal1));
			}
			else
			{
				printf("Error in Python function getMul:\n");
				PyErr_Print();
			}
		}
		else
		{
			printf("Error: function getMult()\n");
			PyErr_Print();
		}
		
		
		//Test de manipulation de vecteurs
		printf("Test vector:\n");
		//Declare vecteurs C++
		std::vector<double> x_vec;
		std::vector<double> y_vec;
		for(int i=0; i< 5; i++)
		{
			x_vec.push_back(i+2.0);
		}
		for(int i=0; i<7; i++)
		{
			y_vec.push_back(i+1.0);
		}
		
		//Alloue les tuples ("vecteurs") Pythons qui vont servir au transfert
		px_vector = PyTuple_New(x_vec.size());
		py_vector = PyTuple_New(y_vec.size());
		
		//Transfère les valeurs des vecteurs C++ aux tupples Python
		for(int i = 0; i < x_vec.size(); i++)
		{
			pValue = PyFloat_FromDouble(x_vec[i]);
			PyTuple_SetItem(px_vector, i, pValue);
		}
		for(int i = 0; i < y_vec.size(); i++)
		{
			pValue = PyFloat_FromDouble(y_vec[i]);
			PyTuple_SetItem(py_vector, i, pValue);
		}
		
		//Definie le container final pour Python
		pArgTuple = PyTuple_New(2);
		
		//Empaquetage des 2 vecteurs
		PyTuple_SetItem(pArgTuple, 0, px_vector);
		PyTuple_SetItem(pArgTuple, 1, py_vector);
		
		CPyObject pFunc3 = PyObject_GetAttrString(pModule, "getVector");
		if(pFunc3 && PyCallable_Check(pFunc3))
		{
			//On appelle la fonction python avec notre tuple qui contient les 2 vecteurs
			CPyObject pValue3 = PyObject_CallObject(pFunc3, pArgTuple);
			
			//Test de la réussite de la fonction Python
			//Car sinon aucun message n'est indiqué
			if(pValue3 != NULL)
			{
				px_vector2 = PyTuple_GetItem(pValue3, 0);
				py_vector2 = PyTuple_GetItem(pValue3, 1);
				
				std::cout<< PyFloat_AsDouble(PyTuple_GetItem(px_vector2,3)) << std::endl;			
				std::cout<< PyFloat_AsDouble(PyTuple_GetItem(py_vector2,3)) << std::endl;			
				
				printf("Done\n");
			}
			else
			{
				printf("Error in Python function getVector:\n");
				PyErr_Print();
			}
			
		}
		else
		{
			printf("Error: function getVector()\n");
			PyErr_Print();
		}
			
	}
	else
	{
		printf("ERROR: Module not imported\n");
		PyErr_Print();
	}
	
	//BP TODO: Ajouter les fonctions pour libérer les objets Python proprement à la fin du code et en cas d'erreur

	return 0;
}




//~ #include <stdio.h>
//~ #include <stdlib.h>
//~ #include <Python.h>
//~ #include "pyhelper.hpp"

//~ int main()
//~ {
	//~ char filename[] = "test_py.py";
	//~ FILE* fp; 
	
	//~ CPyInstance hInstance;
	
	//~ CPyObject pName = PyUnicode_FromString("pyemb3");
	//~ CPyObject pModule = PyImport_Import(pName);
	
	//~ if(pModule)
	//~ {
		//~ CPyObject pFunc = PyObject_GetAttrString(pModule, "getInteger");
		//~ if(pFunc && PyCallable_Check(pFunc))
		//~ {
			//~ CPyObject pValue = PyObject_CallObject(pFunc, NULL);

			//~ printf("C: getInteger() = %ld\n", PyLong_AsLong(pValue));
		//~ }
		//~ else
		//~ {
			//~ printf("ERROR: function getInteger()\n");
		//~ }

	//~ }
	//~ else
	//~ {
		//~ printf("ERROR: Module not imported\n");
	//~ }
	
	//~ CPyObject p;
	//~ p = PyLong_FromLong(50);
	//~ printf("Value = %ld\n", PyLong_AsLong(p));
	
	//~ fp = _Py_fopen(filename, "r");
	//~ PyRun_SimpleFile(fp, filename);

	//~ PyRun_SimpleString("print('Hello World from Embedded Python!!!')");
	
	//~ Py_Finalize();

	//~ return 0;
//~ }
