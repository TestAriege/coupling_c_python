#!/usr/bin/env python
#-*- coding: utf-8 -*-

import numpy as np
import os
import sys
from newTest import *
import matplotlib.pyplot as plt


#Fonction pour convertir le vecteur en tuple
def totuple(a):
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a    

def getInteger():
    print('Python function getInteger() called')
    c = 100*50/30
    print("Python c : ",c)
    return c
    
    
def getMult(a,b):
	print("New tests:")
	PATH = os.getcwd()
	print(PATH)
	
	e = printToto()
	
	print('Python mult test:')
	d = 100 * a + b
	print('Python d = ',d)
	return d, a #Renvoye un tuple pour la fonction

def getVector(x,y):
	print('Python function getVector() called')
	x = np.fromiter(x,dtype = np.float)
	y = np.fromiter(y,dtype = np.float)
	print(x)
	print(y)
	plt.plot(x)
	plt.show()
	#Il faut renvoyer un tuple pour pouvoir le remanipuler du côté C++
	return totuple(x),totuple(y)
	
	


if __name__ == "__main__":
	a = printToto()	
	
